# SafeGirl Releases

## Release 0.1
O download do APK da aplicação está disponível neste [link](https://gitlab.com/mateusbzerra/safegirl-releases/blob/master/0.1/app-release.apk)

## Release 0.2
O download do APK da aplicação está disponível neste [link](https://gitlab.com/mateusbzerra/safegirl-releases/blob/master/0.2/app-release.apk)

## Release 1.0.0
O download do APK da aplicação está disponível neste [link](https://gitlab.com/mateusbzerra/safegirl-releases/blob/master/1.0.0/app-release.apk)
